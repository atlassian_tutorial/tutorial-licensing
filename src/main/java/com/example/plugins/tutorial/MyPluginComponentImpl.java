package com.example.plugins.tutorial;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.LicenseError;
import com.atlassian.upm.api.license.entity.PluginLicense;


public class MyPluginComponentImpl implements MyPluginComponent
{
    private final ApplicationProperties applicationProperties;
    private final PluginLicenseManager licenseManager;

    public MyPluginComponentImpl(PluginLicenseManager licenseManager, ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
        this.licenseManager = licenseManager;
    }

    public String getName()
    {
        /** check for a valid license */
        String licenseMessage = "";
        for (PluginLicense pluginLicense : licenseManager.getLicense()) {
            //Check and see if the stored license has an error. If not, it is currently valid.
            if (pluginLicense.getError().isDefined()) {
                    if (pluginLicense.getError().get() == LicenseError.EXPIRED) {
                        licenseMessage += "Admin Info:  The error is: license EXPIRED";
                    } else if (pluginLicense.getError().get() == LicenseError.TYPE_MISMATCH) {
                        licenseMessage += "Admin Info:  The error is: license TYPE MISMATCH";
                    } else if (pluginLicense.getError().get() == LicenseError.USER_MISMATCH) {
                        licenseMessage += "Admin Info:  The error is: license USER_MISMATCH";
                    } else if (pluginLicense.getError().get() == LicenseError.VERSION_MISMATCH) {
                        licenseMessage += "Admin Info:  The error is: license VERSION_MISMATCH";
                    }
                    return licenseMessage;
            } else {
                //A license is currently stored and it is valid.
                System.out.println("License is VALID for myComponent:"+ applicationProperties.getDisplayName());
                continue;
            }
        }
        
        if(null != applicationProperties)
        {
            return "myComponent:" + applicationProperties.getDisplayName();
        }
        
        return "myComponent";
    }
}