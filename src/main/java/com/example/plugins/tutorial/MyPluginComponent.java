package com.example.plugins.tutorial;

public interface MyPluginComponent
{
    String getName();
}